#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <regex>
#include "Escape/Graph.h"
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/EdgeHash.h"
// #include "Escape/DegreeHelper.h"

using namespace std;
using namespace std::chrono;


void usage() {
    printf("\ntest_actual \n\t-i input_file_1[,input_file_2,...]\n");
    printf("\nExample:\n./test_actual -i ca-AstroPh.edges\n");
}


int main(int argc, char *argv[])
{
    if(argc < 5) {
    	cout << argc << endl;
        usage();
        exit(1);
    }

    ofstream of, tab, resf_b; // output file
    vector<string> graphs; // input files
 
	double min_percent = 0, max_percent = 0, base;
	int bp = 0, min_ncoll = 0, max_ncoll = 0, nIter = 0;
    string outputfname;
    VertexIdx MAX_DEG = 0;

    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:m:")) != -1) {
        switch (c) {
            case 'i': {
            	// multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {     
                    string gfile = std::string(token);             
                    graphs.push_back(gfile);
                    printf("%s,",gfile.c_str());
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'm':
                MAX_DEG = atoi(optarg);
                printf("MAX_DEG: %d\n", MAX_DEG);
            break;
        
        }
    }
    printf("*****************************\n\n");

    srand (time(NULL));

    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
                exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);
            
        VertexIdx n = g.nVertices; 
        
        string delimiter = ".";
        string gname = it->substr(0, it->find(delimiter));

        // VertexIdx MAX_DEG = 15000;

    

        ostringstream outfile_b;
        outfile_b << gname << "_" << MAX_DEG << "_a_data";

        cout << "Output file name = " << outfile_b.str() << endl;
        resf_b.open("../results/DegreeMoments/rawdata/actual_inv/" + outfile_b.str());
        if (!resf_b.is_open())
        {
            std::cout << "Could not open data file." << std::endl;
            exit(1);
        }

        vector<VertexIdx> sample;
        sample.resize(MAX_DEG);
        for (int i=0; i<n; i++)
        {
            VertexIdx deg = cg.degree(i);
            sample[deg]++;
        }
        for (int i=0; i<MAX_DEG; i++)
        {
            resf_b << sample[i] << ",";
        }
        resf_b << endl;
        resf_b.close();
        

        delGraph(g);
        delCGraph(cg);
        of.close();
        tab.close();
        
    }
    
        
    return 0;
}


