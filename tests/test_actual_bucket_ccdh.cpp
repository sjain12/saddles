#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <regex>
#include "Escape/Graph.h"
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/EdgeHash.h"
#include "Escape/DegreeBucket.h"

using namespace std;
using namespace std::chrono;


void usage() {
    printf("\ntest_actual \n\t-i input_file_1[,input_file_2,...] -l base\n");
    printf("\nExample:\n./test_actual -i ca-AstroPh.edges -l 1.1\n");
}


int main(int argc, char *argv[])
{
    if(argc < 5) {
    	cout << argc << endl;
        usage();
        exit(1);
    }

    ofstream of, tab, resf_b; // output file
    vector<string> graphs; // input files
 
	double min_percent = 0, max_percent = 0, base;
	int bp = 0, min_ncoll = 0, max_ncoll = 0, nIter = 0;
    string outputfname;

    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:l:")) != -1) {
        switch (c) {
            case 'i': {
            	// multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {     
                    string gfile = std::string(token);             
                    graphs.push_back(gfile);
                    printf("%s,",gfile.c_str());
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'l':
                base = atof(optarg);
                printf("base: %.2f\n", base);
            break;
        
        }
    }
    printf("*****************************\n\n");

    srand (time(NULL));

    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
                exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);
            
        VertexIdx n = g.nVertices; 
        
        string delimiter = ".";
        string gname = it->substr(0, it->find(delimiter));

        double dt = log(n) / log (base);
        int t = dt+1;

        vector<VertexIdx> ds;
        ds.resize(t);
        vector<EdgeIdx> actual;
        actual.resize(t);
        

        for (int i=1; i<=t; i++)
        {   
            ds[i-1] = pow(base, i-1);
        }

        actual = estimate_actual(cg, t, ds, base);

        ostringstream outfile_b;
        outfile_b << gname << "_a_bucket_data";

        cout << "Output file name = " << outfile_b.str() << endl;
        resf_b.open("../results/DegreeMoments/rawdata/bucket_and_actual/actual/" + outfile_b.str());
        if (!resf_b.is_open())
        {
            std::cout << "Could not open data file." << std::endl;
            exit(1);
        }


        resf_b << setw(10) << "buckets,";
                
        for (int i=1; i<=t; i++)
        {
            resf_b << setw(10) << to_string(i) + ",";
        }
        resf_b << endl;
                
        resf_b << setw(10) << "degrees,";
                

        for (int i=0; i<t; i++)
        {
            resf_b << setw(10) << to_string(int(pow(base,i+1))) + ",";
        }
        resf_b << endl;
        
        resf_b << setw(10) << "actual,";
        for (int l=1; l<=t; l++)
        {
            resf_b << setw(10) << to_string(int(actual[l-1])) + ".1,";
        }
        resf_b << endl;
        resf_b.close();
        

        delGraph(g);
        delCGraph(cg);
        of.close();
        tab.close();
        
    }
    
        
    return 0;
}


