#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <regex>
#include "Escape/Graph.h"
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/EdgeHash.h"
// #include "Escape/DegreeHelper.h"

using namespace std;
using namespace std::chrono;


void usage() {
    printf("\ntest_actual \n\t-i input_file_1[,input_file_2,...]\n");
    printf("\nExample:\n./test_actual -i ca-AstroPh.edges\n");
}


int main(int argc, char *argv[])
{
    if(argc < 3) {
    	cout << argc << endl;
        usage();
        exit(1);
    }

    ofstream of, tab, resf_b, resf_b_dd; // output file
    vector<string> graphs; // input files
 
	double min_percent = 0, max_percent = 0, base;
	int bp = 0, min_ncoll = 0, max_ncoll = 0, nIter = 0;
    string outputfname;

    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:")) != -1) {
        switch (c) {
            case 'i': {
            	// multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {     
                    string gfile = std::string(token);             
                    graphs.push_back(gfile);
                    printf("%s,",gfile.c_str());
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

        
        }
    }
    printf("*****************************\n\n");

    srand (time(NULL));

    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
                exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);
            
        VertexIdx n = g.nVertices; 
        VertexIdx m = g.nEdges;

        string delimiter = ".";
        string gname = it->substr(0, it->find(delimiter));

        VertexIdx max_deg = 0;
        VertexIdx MAX_DEG = 200000;

        ostringstream outfile_b;
        outfile_b << gname << "_a_data";

        ostringstream outfile_b_dd;
        outfile_b_dd << gname << "_a_data_dd";

        cout << "Output file name = " << outfile_b.str() << endl;
        resf_b.open("../results/DegreeMoments/rawdata/bucket_and_actual/actual_ccdh/" + outfile_b.str());
        if (!resf_b.is_open())
        {
            std::cout << "Could not open data file." << std::endl;
            exit(1);
        }

        cout << "Output file name = " << outfile_b_dd.str() << endl;
        resf_b_dd.open("../results/DegreeMoments/rawdata/bucket_and_actual/actual_ccdh/" + outfile_b_dd.str());
        if (!resf_b_dd.is_open())
        {
            std::cout << "Could not open data file." << std::endl;
            exit(1);
        }


        vector<VertexIdx> sample;
        sample.resize(MAX_DEG);
        for (int i=0; i<n; i++)
        {
            VertexIdx deg = cg.degree(i);
            sample[deg]++;
            if (deg > max_deg) max_deg = deg;
        }

        


        resf_b << setw(10) << "degrees,";
        resf_b_dd << setw(10) << "degrees,";
                

        for (int i=0; i<=max_deg; i++)
        {
            resf_b << setw(10) << to_string(i) + ",";
            resf_b_dd << setw(10) << to_string(i) + ",";
        }
        resf_b << endl;
        resf_b_dd << endl;
        
        resf_b << setw(10) << "actual,";
        resf_b_dd << setw(10) << "actual,";
        for (int i=0; i<=max_deg; i++)
        {
            // resf_b << setw(10) << to_string(sample[i]) + ".1,";
            resf_b_dd << setw(10) << to_string(sample[i]) + ".1,";
        }

        for (int i=max_deg-1; i>=0; i--)
        {
            sample[i] += sample[i+1];
        }

        for (int i=0; i<=max_deg; i++)
        {
            resf_b << setw(10) << to_string(sample[i]) + ".1,";
            // resf_b_dd << setw(10) << to_string(sample[i]) + ".1,";
        }
        resf_b << endl;
        resf_b.close();
        resf_b_dd << endl;
        resf_b_dd.close();
        
        cout << gname << " avg deg = " << (double) (m*get_mfrac(gname)/n) << endl;

        delGraph(g);
        delCGraph(cg);
        of.close();
        tab.close();
        
    }
    
        
    return 0;
}


