#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <regex>
#include "Escape/Graph.h"
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/EdgeHash.h"
#include "Escape/DegreeVertexSampling.h"
#include "Escape/DegreeHelper.h"

using namespace std;
using namespace std::chrono;

void usage() {
    printf("\ntest_DegreeVertexSampling \n\t-i input_file_1[,input_file_2,...] \n\t -b bp \n\t -p starting_p \n\t -P ending_p \n\t -n nIter \n\t starting_BPcoll \n\t ending_BPcoll\n\t -l base");
    printf("Algorithm samples p\% of the vertices, for starting_p <= p <= ending_p, doubling p every time. Similarly for starting_BPcoll and ending_BPcoll.\n");
    printf("bp = 0 corresponds to model 1 and bp = 2 corresponds to model 2. For model 1, BPcoll values are irrelevant.\n");
    printf("base corresponds to base and determines what values of d we get ccdh for.\n\n");
    printf("\nExample:\n./test_DegreeVertexSampling -i ca-AstroPh.edges -b 1 -p 0.1 -P 1 -n 100 -c 25 -C 25 -l 1.1\n");
}

int main(int argc, char *argv[])
{
    if(argc < 17) {
    	cout << argc << endl;
        usage();
        exit(1);
    }
    // initialize_mfrac();

    ofstream of, tab, resf_b; // output file
    vector<string> graphs; // input files
 
	double min_percent = 0, max_percent = 0, base;
	int bp = 0, min_ncoll = 0, max_ncoll = 0, nIter = 0;
    string outputfname;

    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:b:n:c:C:p:P:l:")) != -1) {
        switch (c) {
            case 'i': {
            	// multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {     
                    string gfile = std::string(token);             
                    graphs.push_back(gfile);
                    printf("%s,",gfile.c_str());
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'b':
                bp = atof(optarg);
                printf("bp: %d\n", bp);
            break;

            case 'p':
                min_percent = atof(optarg);
                printf("min_percent: %.2f\n", min_percent);
            break;

            case 'P':
                max_percent = atof(optarg);
                printf("max_percent: %.2f\n", max_percent);
            break;


            case 'n':
                nIter = atoi(optarg);
                printf("nIter: %d\n", nIter);
            break;

            case 'c':
                min_ncoll = atoi(optarg);
                printf("min_ncoll: %d\n", min_ncoll);
            break;

            case 'C':
                max_ncoll = atoi(optarg);
                printf("max_ncoll: %d\n", max_ncoll);
            break;

            case 'l':
                base = atof(optarg);
                printf("base: %.2f\n", base);
            break;

        }
    }
    printf("*****************************\n\n");

    srand (time(NULL));

    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
                exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);

        string delimiter = ".";
        string gname = it->substr(0, it->find(delimiter));
            
        VertexIdx n = g.nVertices; 
        EdgeIdx m = g.nEdges;

        double dt = log(n) / log (base);
        int t = dt+1;
        cout << "t = " << t << endl;

        

        ostringstream tabfname;
        tabfname << "../results/DegreeMoments/stats_and_table/vertex_sampling/" << gname << "_" << bp << "_" << min_percent << "_" << max_percent << "_" << nIter << "_" << min_ncoll << "_" << max_ncoll << "_" << base << "_vs_table";
        
        tab.open(tabfname.str());
        if (!tab.is_open())
        {
            std::cout << "Could not open output file." << std::endl;
            exit(1);
        }

        ostringstream ofname;
        ofname << "../results/DegreeMoments/stats_and_table/vertex_sampling/" << gname << "_" << bp << "_" << min_percent << "_" << max_percent << "_" << nIter << "_" << min_ncoll << "_" << max_ncoll << "_" << base << "_vs_stats" ;
        of.open(ofname.str());
        if (!of.is_open())
        {
            std::cout << "Could not open output file." << std::endl;
            exit(1);
        }

        tab << "nsamples, ";
        for (int ncoll = min_ncoll; ncoll <= max_ncoll; ncoll = ncoll * 2)
            tab << ncoll << " ,";
        tab << endl;
        

        for (double percent = min_percent; percent <= max_percent; percent = percent * sqrt(2))
        {
            tab << percent << " , ";
            for (int ncoll = min_ncoll; ncoll <= max_ncoll; ncoll = ncoll * 2)
            {
                
                ostringstream outfile_b;
                outfile_b << gname << "_" << bp << "_" << percent << "_" << nIter << "_" << ncoll << "_" << base << "_vs_data";

                cout << "Output file name = " << outfile_b.str() << endl;
                resf_b.open("../results/DegreeMoments/rawdata/vertex_sampling/" + outfile_b.str());
                if (!resf_b.is_open())
                {
                    std::cout << "Could not open data file." << std::endl;
                    exit(1);
                }

        

                of << "graph = " << *it << endl;
        	  	of << "vertices = " << n << endl;
        	  	of << "edges = " << g.nEdges << endl;
                of << "bp = " << bp << endl;
                of << "percent = " << percent << endl;
                of << "nIter = " << nIter << endl;
                of << "ncoll = " << ncoll << endl;
                    
                vector< vector <double> > ests_bucket;
                vector <double> ret_bucket;
                ests_bucket.resize(t);
                ret_bucket.resize(t);
                
                vector<VertexIdx> ds;
                ds.resize(t);
                
                resf_b << setw(10) << "buckets,";
                
                for (int i=1; i<=t; i++)
                {
                    resf_b << setw(10) << to_string(i) + ",";

                    ests_bucket[i-1].resize(nIter);
                    
                    ds[i-1] = pow(base, i-1);
                }
                resf_b << endl;
                
                resf_b << setw(10) << "degrees,";
                

                for (int i=0; i<t; i++)
                {
                    resf_b << setw(10) << to_string(int(pow(base,i+1))) + ",";
                }
                resf_b << endl;
                
             
                int rq = percent * n / 100;
                of << "r+q=" << rq << endl;
                vector<double> tot_nSamples_bucket;
                tot_nSamples_bucket.resize(nIter);
                
                for (int i=0; i<nIter; i++)
                {
                    resf_b << setw(10) << to_string(i+1) + "_vs,";
                
                    double nSamples = 0;

                    ret_bucket = estimate_vertex_sampling(cg, rq, t, ds, nSamples, ncoll, bp, base);
                    nSamples += rq;
                    cout << "nSamples = " << nSamples << endl;
                    tot_nSamples_bucket[i] = nSamples;

                    
                    for (int j=0; j<t; j++)
                    {
                        resf_b << setw(10) << to_string(int(round(ret_bucket[j]))) + ".1,";
                        ests_bucket[j][i] = ret_bucket[j];
                    }
                    resf_b << endl;
                    
                }  
                
                sort(tot_nSamples_bucket.begin(), tot_nSamples_bucket.end());
                VertexIdx med_nSamples = tot_nSamples_bucket[(nIter-1)/2];
                
                of << "min nSamples_bucket used = " << tot_nSamples_bucket[0] << endl;
                of << "max nSamples_bucket used = " << tot_nSamples_bucket[nIter-1] << endl;
                of << "median nSamples_bucket used = " << tot_nSamples_bucket[(nIter-1)/2] << endl;
                of << "% total samples = " << (double) med_nSamples * 100 / (m * get_mfrac(gname)) << endl;

                cout << "min nSamples_bucket used = " << tot_nSamples_bucket[0] << endl;
                cout << "max nSamples_bucket used = " << tot_nSamples_bucket[nIter-1] << endl;
                cout << "median nSamples_bucket used = " << tot_nSamples_bucket[(nIter-1)/2] << endl;
                cout << "% total samples (as a percentage of m) = " << (double) med_nSamples * 100 / (m * get_mfrac(gname)) << endl;

                of << setw(30) << "range," << setw(20) << "bucket," << setw(20) << "algo," << setw(20) << "min," << setw(20) << "1st quartile," << setw(20) << "3rd quartile," << setw(20) << "max," << setw(20) << "median," << endl; 
                

                for (int l=1; l<=t; l++)
                {
                    sort(ests_bucket[l-1].begin(), ests_bucket[l-1].end());
                    
                    std::ostringstream ss;
                    ss << "[" << pow(base, l-1) << ":" << pow(base, l) << "),";
                    of << setw(30) << ss.str() << setw(20) << to_string(l) + ".1," << setw(20) << "bE," << setw(20) << to_string(int(ests_bucket[l-1][0])) + ".1,";
                    of << setw(20) << to_string(int(ests_bucket[l-1][nIter/4])) + ".1," << setw(20) << to_string(int(ests_bucket[l-1][3*nIter/4])) + ".1," << setw(20) << to_string(int(ests_bucket[l-1][nIter-1])) + ".1," << setw(20) << to_string(int(ests_bucket[l-1][nIter/2])) + ".1," << endl;

                }
                
                
                tab << med_nSamples << ", ";
                of << endl;
                resf_b.close();
            }
            tab << endl;
        }

        delGraph(g);
        delCGraph(cg);
        of.close();
        tab.close();
        
    }
    
        
    return 0;
}


