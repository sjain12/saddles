#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <regex>
#include "Escape/Graph.h"
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/EdgeHash.h"
#include "Escape/DegreeEgocentric.h"
#include "Escape/DegreeHelper.h"

using namespace std;
using namespace std::chrono;


void usage() {
    printf("\ntest_DegreeEgoCentric \n\t-i input_file_1[,input_file_2,...] \n\t -p starting_p \n\t -P ending_p \n\t -n nIter \n\t -m MAX_DEG \n");
    printf("Algorithm samples p\% of the vertices, for starting_p <= p <= ending_p, doubling p every time. \n");
    printf("\nExample:\n./test_DegreeEgoCentric -i ca-AstroPh.edges -p 0.1 -P 1 -n 100 -m 1000\n");
}


int main(int argc, char *argv[])
{
    if(argc < 11) {
    	cout << argc << endl;
        usage();
        exit(1);
    }

    ofstream of, tab, resf_b, resf_ne; // output file
    vector<string> graphs; // input files
 
	double min_percent = 0, max_percent = 0, base;
	int bp = 0, min_ncoll = 0, max_ncoll = 0, nIter = 0;
    string outputfname;
    VertexIdx MAX_DEG = 0;

    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:p:n:P:m:")) != -1) {
        switch (c) {
            case 'i': {
            	// multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {     
                    string gfile = std::string(token);             
                    graphs.push_back(gfile);
                    printf("%s,",gfile.c_str());
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'p':
                min_percent = atof(optarg);
                printf("min_percent: %.2f\n", min_percent);
            break;

            case 'P':
                max_percent = atof(optarg);
                printf("max_percent: %.2f\n", max_percent);
            break;


            case 'n':
                nIter = atoi(optarg);
                printf("nIter: %d\n", nIter);
            break;

            case 'm':
                MAX_DEG = atoi(optarg);
                printf("MAX_DEG: %d\n", MAX_DEG);
            break;

        }
    }
    printf("*****************************\n\n");

    srand (time(NULL));

    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
                exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);
            
        VertexIdx n = g.nVertices; 
        
        string delimiter = ".";
        string gname = it->substr(0, it->find(delimiter));

        ostringstream tabfname;
        tabfname << "../results/DegreeMoments/stats_and_table/egocentric_sampling/" << gname << "_" << min_percent << "_" << max_percent << "_" << nIter << "_" << "_e_table";
        
        tab.open(tabfname.str());
        if (!tab.is_open())
        {
            std::cout << "Could not open output file." << std::endl;
            exit(1);
        }

        ostringstream ofname;
        ofname << "../results/DegreeMoments/stats_and_table/egocentric_sampling/" << gname << "_" << min_percent << "_" << max_percent << "_" << nIter << "_e_stats" ;
        of.open(ofname.str());
        if (!of.is_open())
        {
            std::cout << "Could not open output file." << std::endl;
            exit(1);
        }

        vector<double> tot_nSamples;
        tot_nSamples.resize(nIter);

        tab << "percent , median nSamples " << endl;
        for (double percent = min_percent; percent <= max_percent; percent = percent * sqrt(2))
        {
            double prob = percent / 100;
            double nSamples = 0;

            ostringstream outfile_b;
            outfile_b << gname << "_" << percent << "_" << nIter << "_" << MAX_DEG << "_e_data";

            cout << "Output file name = " << outfile_b.str() << endl;
            resf_b.open("../results/DegreeMoments/rawdata/egocentric_sampling/" + outfile_b.str());
            if (!resf_b.is_open())
            {
                std::cout << "Could not open data file." << std::endl;
                exit(1);
            }

            ostringstream outfile_ne;
            outfile_ne << gname << "_" << percent << "_" << nIter << "_e_naive_data";

            cout << "Output file name = " << outfile_ne.str() << endl;
            resf_ne.open("../results/DegreeMoments/rawdata/egocentric_sampling/" + outfile_ne.str());
            if (!resf_ne.is_open())
            {
                std::cout << "Could not open data file." << std::endl;
                exit(1);
            }

            of << "graph = " << *it << endl;
            of << "vertices = " << n << endl;
            of << "edges = " << g.nEdges << endl;
            of << "percent = " << percent << endl;
            of << "nIter = " << nIter << endl;

            

            for (int run=0; run<nIter; run++)
            {
                nSamples = 0;
                vector<VertexIdx> sample = estimate_egocentric(cg, prob, MAX_DEG, nSamples);
                tot_nSamples[run] = nSamples;
                // cout << "After calling estimate_induced" << endl;
                for (int i=0; i<MAX_DEG; i++)
                {
                    resf_b << sample[i] << ",";
                }
                resf_b << endl;

                for (int i=0; i<MAX_DEG; i++)
                {
                    resf_ne << round((double)sample[i]*n/(double)nSamples) << ",";
                }
                resf_ne << endl;
            }

            sort(tot_nSamples.begin(), tot_nSamples.end());
            VertexIdx med_nSamples = tot_nSamples[(nIter-1)/2];
            of << "median nSamples = " << med_nSamples << endl;
            tab << percent << " , " << med_nSamples << endl;
            resf_b.close();
            resf_ne.close();
        }

        delGraph(g);
        delCGraph(cg);
        of.close();
        tab.close();
        
    }
    
        
    return 0;
}


