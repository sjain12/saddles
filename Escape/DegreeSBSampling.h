#ifndef ESCAPE_DEGREE_SB_H_
#define ESCAPE_DEGREE_SB_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"

using namespace Escape;
using namespace std;


vector<double> estimate_sb_sampling(CGraph &cg, double r, int t, vector<VertexIdx> &ds, double &nSamples, int nColl, int BP, double base)
{
	vector<double> ests;
	ests.resize(t);
	double n = cg.nVertices;

	vector <double> x;
	double D = 0;
	x.resize(t);

	vector<VertexIdx> bucket_counts(t,0);

	map<VertexIdx, VertexIdx> estimated_degrees;
	map<VertexIdx, int> explored;
	VertexIdx found_already = 0;

	double deg = 0;
	vector <double> incorrectly_added, incorrectly_deleted, original_samples;
	incorrectly_added.resize(t);
	incorrectly_deleted.resize(t);
	original_samples.resize(t);
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng
    int unique_samples = 0;

    std::uniform_int_distribution<int> distr(0,n-1);
	double total_samples = 0;
	while(total_samples<r)
	{
		VertexIdx v = distr(rng);
		// pick a vertex uar. bin it
		// look at all its neighbors. bin them
		// scale the dd
		total_samples++;
		if (explored.find(v) == explored.end())
		{
			explored[v] = 1;			

			if (estimated_degrees.find(v) == estimated_degrees.end()) 
			{
				if (BP)
					deg = degree_BP(cg, v, nSamples, nColl); 
				else
					deg = cg.degree(v);
				estimated_degrees[v] = deg;

				int bucket_actual = floor(log(cg.degree(v)) / log(base));
				original_samples[bucket_actual]++;
				int bucket_est = floor(log(round(deg)) / log(base)); 
				if ((bucket_est != bucket_actual))
				{
					incorrectly_added[bucket_est]++;
					incorrectly_deleted[bucket_actual]++;
				}
				bucket_counts[bucket_est]++;
				unique_samples++;
			}

			for (int j=0; j<cg.degree(v); j++)
			{

				VertexIdx nbr = cg.nbors[cg.offsets[v]+j];
				total_samples++;
				
				if (estimated_degrees.find(nbr) == estimated_degrees.end()) 
				{
				
					if (BP)
						deg = degree_BP(cg, nbr, nSamples, nColl); 
					else
						deg = cg.degree(nbr);
					estimated_degrees[nbr] = deg;

					int bucket_actual = floor(log(cg.degree(nbr)) / log(base));
					original_samples[bucket_actual]++;
					int bucket_est = floor(log(round(deg)) / log(base)); 
					if ((bucket_est != bucket_actual))
					{
						incorrectly_added[bucket_est]++;
						incorrectly_deleted[bucket_actual]++;
					}
					bucket_counts[bucket_est]++;
					unique_samples++;
				}
			}
		
		}
		
	}

	cout << "actual samples = " << r << endl;
	for (int j=0; j<t; j++)
	{
		cout << "j = " << j << " original samples = " << original_samples[j] << " incorrectly_added = " << incorrectly_added[j] << " incorrectly_deleted = " << incorrectly_deleted[j] << endl;
	}

	for (int i=t-2; i>=0; i--)
		bucket_counts[i] += bucket_counts[i+1]; 

	for (int i=0; i<t; i++)
		ests[i] = bucket_counts[i] * n / unique_samples; 
	
	return ests;
}

/*
vector<double> estimate_sb_sampling(CGraph &cg, double r, int t, vector<VertexIdx> &ds, double &nSamples, int nColl, int BP, double base)
{
	vector<double> ests;
	ests.resize(t);
	double n = cg.nVertices;

	vector <double> x;
	double D = 0;
	x.resize(t);

	vector<VertexIdx> bucket_counts(t,0);

	map<VertexIdx, VertexIdx> estimated_degrees;
	map<VertexIdx, int> explored;
	VertexIdx found_already = 0;

	double deg = 0;
	vector <double> incorrectly_added, incorrectly_deleted, original_samples;
	incorrectly_added.resize(t);
	incorrectly_deleted.resize(t);
	original_samples.resize(t);
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng
    int unique_samples = 0;

    std::uniform_int_distribution<int> distr(0,n-1);
	double total_samples = 0;
	// while(unique_samples<r)
	while(unique_samples<r)
	{
		VertexIdx v = distr(rng);
		// pick a vertex uar. bin it
		// look at all its neighbors. bin them
		// scale the dd
		// int bucket_actual = floor(log(cg.degree(v)) / log(base));
		// original_samples[bucket_actual]++;
		unique_samples++;
		if (estimated_degrees.find(v) == estimated_degrees.end()) 
		{
			int bucket_actual = floor(log(cg.degree(v)) / log(base));
			original_samples[bucket_actual]++;	
			if (BP)
				deg = degree_BP(cg, v, nSamples, nColl); 
			else
				deg = cg.degree(v);
			estimated_degrees[v] = deg;

			int bucket_est = floor(log(round(deg)) / log(base)); 
			if ((bucket_est != bucket_actual))
			{
				incorrectly_added[bucket_est]++;
				incorrectly_deleted[bucket_actual]++;
			}
			bucket_counts[bucket_est]++;
			// unique_samples++;

			for (int j=0; j<cg.degree(v); j++)
			{

				VertexIdx nbr = cg.nbors[cg.offsets[v]+j];
				int bucket_actual = floor(log(cg.degree(nbr)) / log(base));
				original_samples[bucket_actual]++;
				
				if (estimated_degrees.find(nbr) == estimated_degrees.end()) 
				{
					int bucket_actual = floor(log(cg.degree(nbr)) / log(base));
					original_samples[bucket_actual]++;	
					if (BP)
						deg = degree_BP(cg, nbr, nSamples, nColl); 
					else
						deg = cg.degree(nbr);
					estimated_degrees[nbr] = deg;


					int bucket_est = floor(log(round(deg)) / log(base)); 
					if ((bucket_est != bucket_actual))
					{
						incorrectly_added[bucket_est]++;
						incorrectly_deleted[bucket_actual]++;
					}
					bucket_counts[bucket_est]++;
					unique_samples++;
				}
			}


		}
		
	}

	cout << "actual samples = " << r << endl;
	for (int j=0; j<t; j++)
	{
		cout << "j = " << j << " original samples = " << original_samples[j] << " incorrectly_added = " << incorrectly_added[j] << " incorrectly_deleted = " << incorrectly_deleted[j] << endl;
	}

	for (int i=t-2; i>=0; i--)
		bucket_counts[i] += bucket_counts[i+1]; 

	for (int i=0; i<t; i++)
		ests[i] = bucket_counts[i] * n / unique_samples; 
	
	return ests;
}
*/
#endif