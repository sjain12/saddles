#ifndef ESCAPE_DEGREE_VERTEX_H_
#define ESCAPE_DEGREE_VERTEX_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"

using namespace Escape;
using namespace std;


vector<VertexIdx> estimate_egocentric(CGraph &cg, double prob, VertexIdx MAX_DEG, double &nSamples)
{
	

	/* sample r vertices
	find degree of each within sampled subgraph
	bin them*/

	map<VertexIdx, VertexIdx> sampled_vertices;
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng
    int unique_samples = 0;
    VertexIdx n = cg.nVertices;

    std::uniform_int_distribution<int> distr(0,100000);
    vector<VertexIdx> sample;
	sample.resize(MAX_DEG);
	// double n = cg.nVertices;

	// double prob = (double) r * 100/ (doube) n;

	for (int i=0; i<n; i++)
	{
		int coin = distr(rng);
		// cout << "coin = " << coin << endl;
		if (coin < (prob*100000)) 
		{
			VertexIdx deg = cg.degree(i);
			sample[deg]++;
			nSamples++;
		}
	}
	
	return sample;
}

#endif