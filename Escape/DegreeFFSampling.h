#ifndef ESCAPE_DEGREE_FF_H_
#define ESCAPE_DEGREE_FF_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"
#include <queue>

using namespace Escape;
using namespace std;


vector<double> estimate_ff_sampling(CGraph &cg, double r, int t, vector<VertexIdx> &ds, double &nSamples, int nColl, int BP, double base)
{
	vector<double> ests;
	ests.resize(t);
	double n = cg.nVertices;

	vector <double> x;
	double D = 0;
	x.resize(t);

	vector<VertexIdx> bucket_counts(t,0);

	map<VertexIdx, VertexIdx> estimated_degrees;
	VertexIdx found_already = 0;

	double deg = 0;
	vector <double> incorrectly_added, incorrectly_deleted, original_samples;
	incorrectly_added.resize(t);
	incorrectly_deleted.resize(t);
	original_samples.resize(t);
	
    int unique_samples = 0, total_samples = 0;

    VertexIdx v = rand() % cg.nVertices;
    if (BP)
		deg = degree_BP(cg, v, nSamples, nColl); 
	else
		deg = cg.degree(v);
	estimated_degrees[v] = deg;

	int bucket_actual = floor(log(cg.degree(v)) / log(base));
	original_samples[bucket_actual]++;

	int bucket_est = floor(log(round(deg)) / log(base)); 
	if ((bucket_est != bucket_actual))
	{
		incorrectly_added[bucket_est]++;
		incorrectly_deleted[bucket_actual]++;
	}
	bucket_counts[bucket_est]++;
	unique_samples++;
	total_samples++;

	double pf = 0.25;
	std::default_random_engine generator;
  	std::geometric_distribution<int> distribution(pf);

  	std::queue<VertexIdx> q;
  	q.push(v);
  	// VertexIdx vertex_samples = 1;
  	

  	while(!q.empty())
  	{
  		if (total_samples > r) break;
  		VertexIdx v = q.front();
  		q.pop();
  		int x = distribution(generator) + 1;
  		vector<VertexIdx> nbrs;
  		// collect unseen nbrs
  		for (int j=0; j<cg.degree(v); j++)
		{
			VertexIdx nbr = cg.nbors[cg.offsets[v]+j];

			if (estimated_degrees.find(nbr) == estimated_degrees.end()) 
				nbrs.push_back(nbr);
		}
		int size = nbrs.size();
		if (size == 0) 
		{
			if (!q.empty())
				continue;
			else
			{
				VertexIdx u = rand() % cg.nVertices;
				total_samples++;
				while(estimated_degrees.find(u) != estimated_degrees.end())
				{ 
					u = rand() % cg.nVertices;
					total_samples++;
				}
			    if (BP)
					deg = degree_BP(cg, u, nSamples, nColl); 
				else
					deg = cg.degree(u);
				estimated_degrees[u] = deg;
				// vertex_samples++;
				int bucket_actual = floor(log(cg.degree(u)) / log(base));
				original_samples[bucket_actual]++;

				int bucket_est = floor(log(round(deg)) / log(base)); 
				if ((bucket_est != bucket_actual))
				{
					incorrectly_added[bucket_est]++;
					incorrectly_deleted[bucket_actual]++;
				}
				bucket_counts[bucket_est]++;
				unique_samples++;
			  	q.push(u);
			}
		}
		else if (size <= x) 
		{
			for (std::vector<VertexIdx>::iterator it = nbrs.begin() ; it != nbrs.end(); ++it)
			{
				q.push(*it);
				unique_samples++;
				total_samples++;
				if (BP)
					deg = degree_BP(cg, *it, nSamples, nColl); 
				else
					deg = cg.degree(*it);
				estimated_degrees[*it] = deg;
				int bucket_actual = floor(log(cg.degree(*it)) / log(base));
				original_samples[bucket_actual]++;
				// vertex_samples++;
				int bucket_est = floor(log(round(deg)) / log(base)); 
				if ((bucket_est != bucket_actual))
				{
					incorrectly_added[bucket_est]++;
					incorrectly_deleted[bucket_actual]++;
				}
				bucket_counts[bucket_est]++;
			}
		}
		else if (size > x)
		{
			for (int j=0; j<x; j++)
			{
				int cur_size = size - j;
				int pos = rand() % cur_size;
				VertexIdx u = nbrs[pos];
				q.push(u);
				nbrs.erase(nbrs.begin() + pos);

				if (BP)
					deg = degree_BP(cg, u, nSamples, nColl); 
				else
					deg = cg.degree(u);
				estimated_degrees[u] = deg;
				unique_samples++;
				total_samples++;
				// vertex_samples++;
				int bucket_actual = floor(log(cg.degree(u)) / log(base));
				original_samples[bucket_actual]++;

				int bucket_est = floor(log(round(deg)) / log(base)); 
				if ((bucket_est != bucket_actual))
				{
					incorrectly_added[bucket_est]++;
					incorrectly_deleted[bucket_actual]++;
				}
				bucket_counts[bucket_est]++;
			}
		}

  		//collect neighbors of v that have not been seen
  		//if number of nbrs < x, add all to Q and to estimate_degrees
  		// if number of neighbors == 0, remove next element from Q
  		// if Q becomes empty, start from another new vertex
  		// if x < nbrs, sample x nbrs u.a.r.
	}

	cout << "actual samples in vertex sampling phase = " << r << endl;
	for (int j=0; j<t; j++)
	{
		cout << "j = " << j << " original samples = " << original_samples[j] << " incorrectly_added = " << incorrectly_added[j] << " incorrectly_deleted = " << incorrectly_deleted[j] << endl;
	}

	for (int i=t-2; i>=0; i--)
		bucket_counts[i] += bucket_counts[i+1]; 

	for (int i=0; i<t; i++)
	{
		ests[i] = bucket_counts[i] * n / unique_samples; 
	}
	
	return ests;
}

#endif