#ifndef ESCAPE_DEGREE_BUCKET_H_
#define ESCAPE_DEGREE_BUCKET_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"

using namespace Escape;
using namespace std;

double weight(CGraph& cg, VertexIdx degu, VertexIdx d)
{
	double wt = 0;
	
	if (degu >= d) 
	{
		wt = 1.0/ (double) degu;
	}
	return wt;
}

vector<double> estimate_bucket(CGraph &cg, double r, double q, int t, vector<VertexIdx> &ds, double &nSamples, int nColl, int BP, double base)
{
	vector<double> ests;
	ests.resize(t);
	double n = cg.nVertices;

	vector <double> x;
	double D = 0;
	x.resize(t);

	vector <VertexIdx> degrees, vertices;
	degrees.resize(r);
	vertices.resize(r);

	vector<VertexIdx> bucket_counts(t,0);
	vector<VertexIdx> bucket_edges(t,0);

	map<VertexIdx, VertexIdx> estimated_degrees;
	VertexIdx found_already = 0;

	double deg = 0;
	vector <double> incorrectly_added, incorrectly_deleted, original_samples;
	incorrectly_added.resize(t);
	incorrectly_deleted.resize(t);
	original_samples.resize(t);
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng

    std::uniform_int_distribution<int> distr(0,n-1);
	for (int i=0; i<r; i++)
	{
		VertexIdx v = distr(rng);
		vertices[i] =  v;
		int bucket_actual = get_bucket(cg.degree(v), t, ds, base);
		original_samples[bucket_actual]++;
		
		if (estimated_degrees.find(v) == estimated_degrees.end()) 
		{
			if (BP)
				deg = degree_BP(cg, v, nSamples, nColl); 
			else
				deg = cg.degree(v);
			estimated_degrees[v] = deg;
		}
		else 
		{			
			deg = estimated_degrees[v];
			found_already++;
		}	

		degrees[i] = deg;
		D += degrees[i];

		int bucket_est = get_bucket(round(deg), t, ds, base);
		if ((bucket_est != bucket_actual))
		{
			incorrectly_added[bucket_est]++;
			incorrectly_deleted[bucket_actual]++;
		}
		bucket_counts[bucket_est]++;
		bucket_edges[bucket_est] += deg;
	}

	cout << "actual samples in vertex sampling phase = " << r << endl;
	for (int j=0; j<t; j++)
	{
		cout << "j = " << j << " original samples = " << original_samples[j] << " incorrectly_added = " << incorrectly_added[j] << " incorrectly_deleted = " << incorrectly_deleted[j] << endl;
	}

	std::random_device rd;
	std::default_random_engine generator (rd());
  	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());

  	vector <double> incorrectly_added_e, incorrectly_deleted_e, original_samples_e;
  	incorrectly_added_e.resize(t);
	incorrectly_deleted_e.resize(t);
	original_samples_e.resize(t);

	for (int i=0; i<q; i++)
	{
		VertexIdx idx = distribution(generator);
		VertexIdx v = vertices[idx];
		VertexIdx u = cg.randomNeighbor(v);
		VertexIdx degv = degrees[idx];
		VertexIdx degu;

		int bucket_actual = get_bucket(cg.degree(u), t, ds, base);
		original_samples_e[bucket_actual]++;
		
		if (estimated_degrees.find(u) == estimated_degrees.end()) 
		{
			if (BP)
				degu = degree_BP(cg, u, nSamples, nColl); 
			else
				degu = cg.degree(u);
			estimated_degrees[u] = degu;
		}
		else 
		{
			degu = estimated_degrees[u];
			found_already++;
		}


		int bucket_est = get_bucket(round(degu), t, ds, base);
		if ((bucket_actual != bucket_est) && (cg.degree(u) > 0))
		{
			incorrectly_added_e[bucket_est]++;
			incorrectly_deleted_e[bucket_actual]++;
		}
		
		for (int j=1; j<=t; j++)		// TO BE OPTIMIZED
		{
			double wt = weight (cg, degu, ds[j-1]);
			x[j-1] = x[j-1] + wt;
		}
		
	}
	cout << "actual samples in edge sampling phase = " << q << endl;
	for (int j=0; j<t; j++)
	{
		cout << "j = " << j << " original samples = " << original_samples_e[j] << " incorrectly_added = " << incorrectly_added_e[j] << " incorrectly_deleted = " << incorrectly_deleted_e[j] << endl;
	}	

	for (int j=1; j<=t; j++)
		ests[j-1] = D * x[j-1] * (double) n/((double) r * (double) q);


	double est = 0;
	VertexIdx s = 0;
	int sbucket = 0;
	double m = cg.nEdges;

	for (int i=t-2; i>=0; i--)
	{
		bucket_counts[i] += bucket_counts[i+1];
	}


	for (int i=0; i<t; i++)
	{
		
		if (bucket_counts[i] > 100)
		{
			ests[i] = bucket_counts[i] * n / r; 
			cout << "Doing vertex sampling for degree:" << ds[i] << endl;
		} 
	}
	
	return ests;
}

vector<EdgeIdx> estimate_actual(CGraph& cg, int t, vector<VertexIdx> &ds, double base)
{
	vector<EdgeIdx> cnt;
	cnt.resize(t);
	for (int i=0; i<cg.nVertices; i++)
	{
		VertexIdx deg = cg.degree(i);
		int bucket = get_bucket(deg, t, ds, base);//floor(log(deg) / log(base));
		cnt[bucket]++; // += deg;
	}
	for (int i=t-2; i>=0; i--)
	{
		cnt[i] += cnt[i+1] ;
	}
	return cnt;

}

#endif