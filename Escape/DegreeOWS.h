#ifndef ESCAPE_DEGREE_VERTEX_H_
#define ESCAPE_DEGREE_VERTEX_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"

using namespace Escape;
using namespace std;


vector<VertexIdx> estimate_ows(CGraph &cg, double prob, VertexIdx max_deg, double &nSamples, double &unique_samples)
{
	

	/* sample r vertices
	find degree of each within sampled subgraph
	bin them*/

	map<VertexIdx, VertexIdx> sampled_vertices;
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng
    // int unique_samples = 0;
    VertexIdx n = cg.nVertices;

    std::uniform_int_distribution<int> distr(0,100000);
    vector<VertexIdx> sample;
	sample.resize(max_deg);

	for (int i=0; i<n; i++)
	{
		int coin = distr(rng);
		// cout << "coin = " << coin << endl;
		if (coin < (prob*100000)) 
		{
			// cout << " prob*100 = " << prob*100000 << endl;
			nSamples++;
			if (sampled_vertices.find(i) == sampled_vertices.end()) 
			{
				unique_samples++;
			}
			VertexIdx deg = cg.degree(i);
			sampled_vertices[i] = deg;
			for (int j=0; j<deg; j++)
	  		{
	  			VertexIdx nbr = cg.nbors[cg.offsets[i]+j];
	  			nSamples++;
	  			if (sampled_vertices.find(nbr) == sampled_vertices.end()) 
				{
					unique_samples++;
				}
	  			sampled_vertices[nbr] = cg.degree(nbr);
	  		}
		}
	}
	// num_unique_samples = 0;
	// total_samples = 0;
	// vector<VertexIdx> degrees_sampled;
	// degrees_sampled.resize(MAX_DEG);
	// int count = 0;
 //  	set<VertexIdx> S;
 //  	max_deg = 0;
  	// for (int i=0; i<r; i++)
  	// {
  	// 	VertexIdx v = distr(rng);
  	// 	VertexIdx degv = cg.degree(v);
  	// 	sampled_vertices[v] = degv;
  	// 	num_samples++;

  	// 	for (int j=0; j<degv; j++)
  	// 	{
  	// 		VertexIdx nbr = cg.nbors[cg.offsets[i]+j];
  	// 		sampled_vertices[nbr] = cg.degree(nbr);
  	// 		num_samples++;
  	// 	}
  	// }
 

	for (std::map<VertexIdx, VertexIdx>::iterator it=sampled_vertices.begin(); it!=sampled_vertices.end(); ++it)
	{
		sample[it->second]++;
	}
	
	return sample;
}

#endif