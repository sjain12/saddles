#ifndef ESCAPE_DEGREE_VERTEX_H_
#define ESCAPE_DEGREE_VERTEX_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include <map>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/DegreeHelper.h"

using namespace Escape;
using namespace std;


vector<VertexIdx> estimate_induced(CGraph &cg, double prob, VertexIdx max_deg, double &nSamples)
{
	

	/* sample r vertices
	find degree of each within sampled subgraph
	bin them*/

	map<VertexIdx, VertexIdx> sampled_vertices;
	std::random_device rd1{}; // use to seed the rng 
    std::mt19937 rng{rd1()}; // rng
    VertexIdx n = cg.nVertices;

    std::uniform_int_distribution<int> distr(0,100000);

	for (int i=0; i<n; i++)
	{
		int coin = distr(rng);
		// cout << "coin = " << coin << endl;
		if (coin < (prob*100000)) 
		{
			nSamples++;
			sampled_vertices[i] = 0;
		}
	}

	for (std::map<VertexIdx, VertexIdx>::iterator it=sampled_vertices.begin(); it!=sampled_vertices.end(); ++it)
	{
		for (std::map<VertexIdx, VertexIdx>::iterator it1=sampled_vertices.begin(); it1!=sampled_vertices.end(); ++it1)
		{
			if (cg.isEdge(it->first, it1->first) > -1)
			{
				sampled_vertices[it->first]++;
			}
		}
	}

	vector<VertexIdx> sample;
	sample.resize(max_deg);

	for (std::map<VertexIdx, VertexIdx>::iterator it=sampled_vertices.begin(); it!=sampled_vertices.end(); ++it)
		sample[it->second]++;


	
	return sample;
}

#endif