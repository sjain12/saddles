#ifndef ESCAPE_DEGREE_HELPER_H_
#define ESCAPE_DEGREE_HELPER_H_

#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"

using namespace Escape;
using namespace std;


int get_bucket(VertexIdx deg, int t, vector<VertexIdx> &ds, double base)
{
	for (int i=t-1; i>=0; i--)
	{
		if (ds[i] <= deg) return i;
	}
}

/*************************************** Birthday Paradox based vertex degree estimator *******************************/

double BP_estimator(double c, double found)
{
	double deg = c*(c-1)/(2*found);
	return deg;
}




double degree_BP(CGraph &cg, VertexIdx v, double &nSamples, int min_nColl)
{
	int placed = 0;
	map<VertexIdx, int> samples;
	int num_2coll = 0;
	double est_d = 0;
	int unique = 0;
	int s = 0;
	while(num_2coll<min_nColl)
	{
		VertexIdx u = cg.randomNeighbor(v);
		map<VertexIdx, int>::const_iterator it = samples.find(u);
		if (it==samples.end()) 
		{
			samples[u] = 1;
			unique++;
		}
		else 
		{
			num_2coll += samples[u];
			samples[u]++;
		}
		s++;
	}

	nSamples += s;

	est_d = s*s/(2.0*num_2coll);
	if (est_d < unique) 
	{
		est_d = unique;
	}
	return est_d;
}

#endif
