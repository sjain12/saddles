#ifndef ESCAPE_DEGREE_MOMENTS_H_
#define ESCAPE_DEGREE_MOMENTS_H_

#include <iostream>
#include <random>
#include <cmath>
#include <set>
#include "Escape/Graph.h"
#include "Escape/Digraph.h"

using namespace Escape;
using namespace std;

#define nCollissions 1.0


/*************************************** Birthday Paradox based vertex degree estimator *******************************/

double BP_estimator(double c)
{
	return (c*c)/(2*nCollissions);
	// c--;
	// return (c*c*2)/3.14;
}

VertexIdx degree_BP(CGraph &cg, VertexIdx v, unsigned long long &nSamples)
{
	vector <VertexIdx> nbrs;
	VertexIdx MAX = 1000;
	nbrs.resize(MAX);
	int c = 0;
	int nIter = 1;
	vector <double> ests;
	ests.resize(nIter);
	// int nCollissions = 1;

	for (int i=0; i<nIter; i++)
	{
		int found = 0;
		for (c=0; c<MAX; c++)
		{
			if (c == MAX-1) 
			{
				MAX *= 2;
				nbrs.resize(MAX);
			}

			VertexIdx u = cg.randomNeighbor(v);
			for (VertexIdx j=0; j<c; j++)
			{
				if (nbrs[j] == u)
				{
					// c++;
					found++;
					break;
				}

			}
			if (found == nCollissions) 
			{
				c++;
				ests[i] = BP_estimator(c); //(double)(c*c)/(2*nCollissions);
				// ests[i] = ((double)(c*c))*2/3.14;
				break;
			}
			nbrs[c] = u;
		}
	}
	sort(ests.begin(), ests.end());
	nSamples += c;
	// cout << "Actual degree = " << cg.degree(v);
	// cout << ", \t BP degree = " << ests[nIter/2] << endl;;
	
	return ests[nIter/2];
}

double lower_deg(CGraph &cg, VertexIdx v, VertexIdx u, unsigned long long &nSamples)
{
	vector <VertexIdx> nbrsu, nbrsv;
	VertexIdx MAX = 1000;

	double est = 0;

	nbrsv.resize(MAX);
	nbrsu.resize(MAX);
	
	// int nCollissions = 4, 
	int c = 0;
	int foundu = 0, foundv = 0;

	for (c=0; c<MAX; c++)
	{
		if (c == MAX-1) 
		{
			MAX *= 2;
			nbrsu.resize(MAX);
			nbrsv.resize(MAX);
		}
		VertexIdx nu = cg.randomNeighbor(u);
		VertexIdx nv = cg.randomNeighbor(v);


		for (int j=0; j<c; j++)
		{
			if (nbrsu[j] == nu)
			{
				foundu++;
				break;
			}
		}
		for (int j=0; j<c; j++)
		{
			if (nbrsv[j] == nv)
			{
				foundv++;
				break;
			}
		}
		if ((foundu == nCollissions) && (foundv < nCollissions)) // v has higher degree
		{
			est = 0;
			break;
		}
		if ((foundu < nCollissions) && (foundv == nCollissions)) // v has lower degree
		{
			c++;
			est = BP_estimator(c); //((double)c*c)/(2*(double)nCollissions);
			break;
		}
		if ((foundv == nCollissions) && (foundu == nCollissions))
		{
			c++;
			if (v < u)
				est = BP_estimator(c); //((double)c*c)/(2*(double)nCollissions);
			break;
		}
		nbrsu[c] = nu;
		nbrsv[c] = nv;

	}
	nSamples += c;
	// cout << "returning est = " << est << endl;
	// cout << "actual degree of v = " << cg.degree(v) << endl;
	// cout << "actual degree of u = " << cg.degree(u) << endl;

	return est;
}

/*************************************** brute force moment calculator *******************************/

double momentCalculator (CGraph& cg, double s)
{
	double m = 0;
	for (int i=0; i<cg.nVertices; i++)
	{
		m += pow(cg.degree(i), s);
	}
	return m / (double) cg.nVertices;
}

/*************************************** DKS *******************************/

double guess_smooth(CGraph& cg, int l, int u, double delta, double epsilon, vector <VertexIdx> degrees, double moment)
{
	vector <VertexIdx> degrees_c;
	degrees_c.resize(cg.nVertices);
	for (int c=1; c<u; c=c*2)//pow(2,c))
	{
		double N = 0;
		for (int i=0; i<cg.nVertices; i++)
			degrees_c[i] = degrees[i] + c;
		// Seed with a real random value, if available
    	std::random_device rd;
		std::default_random_engine generator(rd());
  		std::discrete_distribution<int> distribution (degrees_c.begin(), degrees_c.end());
		int r = log((2/delta) * log2(u/l)) / (2*epsilon*epsilon);
		for (int j=0; j<r; j++)
		{
			VertexIdx v = distribution(generator);
			VertexIdx degv = cg.degree(v);
			if (pow(degv, moment) < c) N++;
		}
		if ((N/(double)r) >= 0.5-epsilon) return c-1;
	}
	return u;
}

double smooth (CGraph& cg, int r, double c, vector <VertexIdx> degrees, double moment)
{
	std::vector <VertexIdx> s;
	s.resize(r);
	std::random_device rd;
	std::default_random_engine generator (rd());
	vector <VertexIdx> degrees_c;
	degrees_c.resize(cg.nVertices);
	for (int i=0; i<cg.nVertices; i++)
		degrees_c[i] = degrees[i] + c;
  	std::discrete_distribution<int> distribution (degrees_c.begin(), degrees_c.end());
  	double N = 0;
	for (int i=0; i<r; i++)
	{
		VertexIdx v = distribution(generator);
		s[i] = v;
	}
	double D = 0;
	for (int i=0; i<r; i++)
	{
		double degu = cg.degree(s[i]);
		N += (pow(degu, moment) / (degu + c));
		D += (1 / (degu + c));
	}
	return (N/D);
}

double DKS(CGraph& cg, ofstream& of, vector <VertexIdx> degrees, int r, double epsilon, double delta, double moment, int flag_BP)
{
	// double d = guess_smooth(cg, 1, pow(100, moment), delta/2, epsilon, degrees, moment);
	// cout << "DKS: d = " << d << endl;
	double d = 50; //pow(50, moment);
	return smooth(cg, r, d, degrees, moment);
		// for (int k=0; k<nIter; k++)
		// of << setw(15) << i << "," << setw(15) << smooth (cg, i, d, degrees) << "," << endl;
}


/*************************************** brute force moment calculator *******************************/

// vector<double> momentEstimator_range(CGraph& cg, VertexIdx r, unsigned int q, double start_moment, double end_moment, double moment_interval)
// {
// 	vector <VertexIdx> degrees, vertices;
// 	degrees.resize(r);
// 	vertices.resize(r);
// 	double D = 0;
// 	vector<double> x;
// 	int num_moments = ((end_moment - start_moment) / moment_interval) + 1;
// 	x.resize(num_moments);
	
// 	for (int i=0; i<r; i++)
// 	{
// 		vertices[i] = rand() % cg.nVertices;
// 		degrees[i] = cg.degree(vertices[i]);
// 		D += degrees[i]; 
// 	}

// 	std::default_random_engine generator;
//   	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());
// 	for (int i=0; i<q; i++)
// 	{
// 		VertexIdx v = vertices[distribution(generator)];
// 		VertexIdx u = cg.randomNeighbor(v);
// 		EdgeIdx degu = cg.degree(u), degv = cg.degree(v);
// 		if ((degv < degu) || ((degv == degu) && (v < u))) 
// 		{
// 			for (int j=0; j<num_moments; j++)
// 			{
// 				double s = start_moment + (j * moment_interval);
// 				x[j] += (D * (pow(degu, s-1) + pow(degv, s-1)) / ((double) r * (double) q)); 
// 			}
// 		}
// 	}

// 	return x;
// }



// VertexIdx lower_deg_vertex(CGraph &cg, VertexIdx v, VertexIdx u)
// {
// 	vector <VertexIdx> nbrsu, nbrsv;
// 	VertexIdx MAX = 1000;

// 	nbrsv.resize(MAX);
// 	nbrsu.resize(MAX);
	

// }

// vector<double> momentEstimator_range_BP(CGraph& cg, VertexIdx r, unsigned int q, double start_moment, double end_moment, double moment_interval)
// {
// 	vector <VertexIdx> degrees, vertices;
// 	degrees.resize(r);
// 	vertices.resize(r);
// 	double D = 0;
// 	vector<double> x;
// 	int num_moments = ((end_moment - start_moment) / moment_interval) + 1;
// 	x.resize(num_moments);
	
// 	for (int i=0; i<r; i++)
// 	{
// 		vertices[i] = rand() % cg.nVertices;
// 		degrees[i] = degree_BP(cg, vertices[i]);
// 		//degrees[i] = cg.degree(vertices[i]);
// 		D += degrees[i]; 
// 	}

// 	std::default_random_engine generator;
//   	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());
// 	for (int i=0; i<q; i++)
// 	{
// 		VertexIdx Idx = distribution(generator);
// 		VertexIdx v = vertices[Idx];
// 		VertexIdx u = cg.randomNeighbor(v);
// 		EdgeIdx degu = degree_BP(cg, u);
// 		EdgeIdx degv = degrees[Idx];
// 		if ((degv < degu) || ((degv == degu) && (v < u))) 
// 		{
// 			for (int j=0; j<num_moments; j++)
// 			{
// 				double s = start_moment + (j * moment_interval);
// 				x[j] += (D * (pow(degu, s-1) + pow(degv, s-1)) / ((double) r * (double) q)); 
// 			}
// 		}
// 	}

// 	return x;
// }


double momentEstimator_sampling(CGraph& cg, double r, double moment, int flag_BP, unsigned long long &nSamples)
{
	// vector<double> x;
	// int num_moments = ((end_moment - start_moment) / moment_interval) + 1;
	// x.resize(num_moments);
	double x = 0;

	for (int i=0; i<r; i++)
	{
		VertexIdx v = rand() % cg.nVertices;
		VertexIdx degv = 0;
		if (flag_BP == 1)
			degv = degree_BP(cg, v, nSamples);
		else
			degv = cg.degree(v);
		x += (pow(degv, moment));
	}

	return x / r;
}

double momentEstimator(CGraph& cg, VertexIdx r, unsigned long long q, double s, int flag_BP, unsigned long long &nSamples)
{
	vector <VertexIdx> degrees, vertices;
	degrees.resize(r);
	vertices.resize(r);
	double D = 0;
	double x = 0.0;
	
	for (int i=0; i<r; i++)
	{
		vertices[i] = rand() % cg.nVertices;
		if (flag_BP == 1)
			degrees[i] = degree_BP(cg, vertices[i], nSamples);
		else
			degrees[i] = cg.degree(vertices[i]);
		D += degrees[i]; 
	}

	std::default_random_engine generator;
  	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());
	for (int i=0; i<q; i++)
	{
		VertexIdx Idx = distribution(generator);
		VertexIdx v = vertices[Idx];
		VertexIdx u = cg.randomNeighbor(v);
		VertexIdx degu = 0;
		if (flag_BP == 1)
			degu = degree_BP(cg, u, nSamples);
		else 
			degu = cg.degree(u);
		EdgeIdx degv = degrees[Idx];

		// VertexIdx v = vertices[distribution(generator)];
		// VertexIdx u = cg.randomNeighbor(v);
		// EdgeIdx degu = cg.degree(u), degv = cg.degree(v);
		if ((degv < degu) || ((degv == degu) && (v < u))) 
			x += (pow(degu, s-1) + pow(degv, s-1)); 
	}

	return D*x/((double) r * (double) q);
}

// double momentEstimator_BP(CGraph& cg, VertexIdx r, unsigned long long q, double s)
// {
// 	vector <VertexIdx> degrees, vertices;
// 	degrees.resize(r);
// 	vertices.resize(r);
// 	double D = 0;
// 	double x = 0.0;
	
// 	for (int i=0; i<r; i++)
// 	{
// 		vertices[i] = rand() % cg.nVertices;
// 		degrees[i] = degree_BP(cg, vertices[i]); //cg.degree(vertices[i]);
// 		D += degrees[i]; 
// 	}

// 	std::default_random_engine generator;
//   	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());
// 	for (int i=0; i<q; i++)
// 	{

// 		VertexIdx Idx = distribution(generator);
// 		VertexIdx v = vertices[Idx];
// 		VertexIdx u = cg.randomNeighbor(v);
// 		EdgeIdx degu = degree_BP(cg, u);
// 		EdgeIdx degv = degrees[Idx];

// 		if ((degv < degu) || ((degv == degu) && (v < u))) 
// 			x += (pow(degu, s-1) + pow(degv, s-1)); 
// 	}

// 	return D*x/((double) r * (double) q);
// }

double avgDeg_OrderBased (CGraph& cg, VertexIdx r, double moment, int flag_BP, int flag_comp, unsigned long long &nSamples) //Sesh's algorithm, r = number of samples
{
	double x = 0.0;
	EdgeIdx degu = 0, degv = 0;
	
	for (int i=0; i<r; i++)
	{
		VertexIdx v = rand() % cg.nVertices;
		VertexIdx u = cg.randomNeighbor(v);
		if (flag_BP == 1)
		{
			if (flag_comp == 1)
			{
				degv = lower_deg(cg, v, u, nSamples); // lower_deg should return 0 if v's out deg is not less than u's out deg.
				// degu =  degv+1;
				x += (2*pow(degv, moment));
			}
			else
			{
				degu = degree_BP(cg, u, nSamples);
				degv = degree_BP(cg, v, nSamples);
				if ((degv < degu) || ((degv == degu) && (v < u))) 
					x += (2*pow(degv, moment));
			}
		}
		else
		{
			degu = cg.degree(u);
			degv = cg.degree(v);
			if ((degv < degu) || ((degv == degu) && (v < u))) 
					x += (2*pow(degv, moment));
		}

		// if ((degv < degu) || ((degv == degu) && (v < u)) || (flag_comp == 1)) 
		// 	x += (2*pow(degv, moment));  //TODO: CHECK THIS
	}

	double mean = x/(double) r; 

	// cout << "x = " << x;
	// cout << " r = " << r;
	// cout << " mean = " << mean << endl;
	return mean;
}

// double avgDeg_OrderBased_BP (CGraph& cg, VertexIdx r, double moment) //Sesh's algorithm, r = number of samples
// {
// 	double x = 0.0;
	
// 	for (int i=0; i<r; i++)
// 	{
// 		VertexIdx v = rand() % cg.nVertices;
// 		VertexIdx u = cg.randomNeighbor(v);
// 		EdgeIdx degu = degree_BP(cg, u), degv = degree_BP(cg, v);

// 		if ((degv < degu) || ((degv == degu) && (v < u))) 
// 			x += (2*pow(degv, moment)); 
// 	}

// 	return x/(double) r;
// }


// double avgDeg_OrderBased (CGraph& cg, VertexIdx r, unsigned long long q) //Sesh's algorithm, r = number of samples
// {
// 	vector <VertexIdx> degrees, vertices;
// 	degrees.resize(r);
// 	vertices.resize(r);
// 	double D = 0;
// 	double x = 0.0;
	
// 	for (int i=0; i<r; i++)
// 	{
// 		vertices[i] = rand() % cg.nVertices;
// 		degrees[i] = cg.degree(vertices[i]);
// 		D += degrees[i]; 
// 	}

// 	std::default_random_engine generator;
//   	std::discrete_distribution<int> distribution (degrees.begin(), degrees.end());
// 	for (int i=0; i<q; i++)
// 	{
// 		VertexIdx v = vertices[distribution(generator)];
// 		VertexIdx u = cg.randomNeighbor(v);

// 		EdgeIdx degu = cg.degree(u), degv = cg.degree(v);

// 		if ((degv < degu) || ((degv == degu) && (v < u))) 
// 			x += (2*degv); 
// 	}

// 	return D*x/((double) r * (double) q);
// }




#endif
